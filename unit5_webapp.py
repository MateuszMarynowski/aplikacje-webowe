from flask import Flask, render_template, redirect, request
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime
import statistics

app = Flask(__name__)
#app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///:memory:'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///formdata.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = 'True'

db = SQLAlchemy(app)

class Formdata(db.Model):
    __tablename__ = 'form'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    age = db.Column(db.String, nullable=False)
    degree= db.Column(db.Integer)
    field=db.Column(db.String)
    study_type=db.Column(db.Integer)
    sex=db.Column(db.Integer)
    average=db.Column(db.Integer)
    science_club=db.Column(db.Integer)
    study_time=db.Column(db.Integer)
    hobby_time=db.Column(db.Integer)
    free_time=db.Column(db.Integer)
    work=db.Column(db.Integer)
    work_time=db.Column(db.Integer)
    type_contract=db.Column(db.Integer)
    work_vs_study=db.Column(db.Integer)
    reason_of_work=db.Column(db.Integer)
    work_why_not_yes=db.Column(db.Integer)
    future_work=db.Column(db.Integer)
    study_and_work=db.Column(db.Integer)
        

    def __init__(self,
                 age,
                 degree,
                 field,
                 study_type,
                 sex,
                 average,
                 science_club,
                 study_time,
                 hobby_time,
                 free_time,
                 work,
                 work_time,
                 type_contract,
                 work_vs_study,
                 reason_of_work,
                 work_why_not_yes,
                 future_work,
                 study_and_work):
        self.age=age
        self.degree=degree
        self.field=field
        self.study_type=study_type
        self.sex=sex
        self.average=average
        self.science_club=science_club
        self.study_time=study_time
        self.hobby_time=hobby_time
        self.free_time=free_time
        self.work=work
        self.work_time=work_time
        self.type_contract=type_contract
        self.work_vs_study=work_vs_study
        self.reason_of_work=reason_of_work
        self.work_why_not_yes=work_why_not_yes
        self.future_work=future_work
        self.study_and_work=study_and_work
        
db.create_all()


@app.route("/")
def welcome():
    return render_template('welcome.html')

@app.route("/form")
def show_form():
    return render_template('form.html')

@app.route("/raw")
def show_raw():
    fd = db.session.query(Formdata).all()
    return render_template('raw.html', formdata=fd)

@app.route("/save", methods=['POST'])
def save():
    # Get data from FORM
    age = request.form['age']
    degree = request.form['degree']
    field = request.form['field']
    study_type = request.form['study_type']
    sex = request.form['sex']
    average = request.form['average']
    science_club = request.form['science_club']
    study_time = request.form['study_time']
    hobby_time = request.form['hobby_time']
    free_time = request.form['free_time']
    work = request.form['work']
    work_time = request.form['work_time']
    type_contract = request.form['type_contract']
    work_vs_study = request.form['work_vs_study']
    reason_of_work = request.form['reason_of_work']
    work_why_not_yes = request.form['work_why_not_yes']
    future_work = request.form['future_work']
    study_and_work = request.form['study_and_work']
    

    # Save the data
    fd = Formdata(age,
                  degree,
                  field,
                  study_type,
                  sex,
                  average,
                  science_club,
                  study_time,
                  hobby_time,
                  free_time,
                  work,
                  work_time,
                  type_contract,
                  work_vs_study,
                  reason_of_work,
                  work_why_not_yes,
                  future_work,
                  study_and_work)

    db.session.add(fd)
    db.session.commit()

    return render_template('save.html')

@app.route("/result")
def show_result():
    fd_list = db.session.query(Formdata).all()

    # SEX
    women = 0
    men = 0
    others = 0
    for el in fd_list:
        if el.sex == 1:
            women += 1
        elif el.sex == 2:
            men += 1
        elif el.sex == 3:
            others += 1
    # Prepare data for google charts
    sex = [['Kobiety', women], ['Meżczyźni', men], ['Inni', others]]

    # TYPE
    stacjonarne = 0
    niestacjonarne = 0
    for el in fd_list:
        if el.study_type == 1:
            stacjonarne += 1
        else:
            niestacjonarne += 1
    # Prepare data for google charts
    type = [['Stacjonarne', stacjonarne], ['Niestacjonarne', niestacjonarne]]

    # STUDY
    zero_to_three_FT = 0
    three_to_six_FT = 0
    six_to_nine_FT = 0
    nine_to_twelve_FT = 0
    zero_to_three_PT = 0
    three_to_six_PT = 0
    six_to_nine_PT = 0
    nine_to_twelve_PT = 0
    for el in fd_list:
        if el.study_time == 1:
            if el.study_type == 1:
                zero_to_three_FT += 1
            else:
                zero_to_three_PT += 1
        elif el.study_time == 2:
            if el.study_type == 1:
                three_to_six_FT += 1
            else:
                three_to_six_PT += 1
        elif el.study_time == 3:
            if el.study_type == 1:
                six_to_nine_FT += 1
            else:
                six_to_nine_PT += 1
        elif el.study_time == 4:
            if el.study_type == 1:
                nine_to_twelve_FT += 1
            else:
                nine_to_twelve_PT += 1

    # Prepare data for google charts
    study = [['0-3', zero_to_three_FT, zero_to_three_PT], ['3-6', three_to_six_FT, three_to_six_PT], ['6-9', six_to_nine_FT, six_to_nine_PT], ['9-12', nine_to_twelve_FT, nine_to_twelve_PT]]

    # free_time
    free_zero_to_three_FT = 0
    free_three_to_six_FT = 0
    free_six_to_nine_FT = 0
    free_nine_to_twelve_FT = 0
    free_zero_to_three_PT = 0
    free_three_to_six_PT = 0
    free_six_to_nine_PT = 0
    free_nine_to_twelve_PT = 0
    for el in fd_list:
        if el.hobby_time == 1:
            if el.study_type == 1:
                free_zero_to_three_FT += 1
            else:
                free_zero_to_three_PT += 1
        elif el.hobby_time == 2:
            if el.study_type == 1:
                free_three_to_six_FT += 1
            else:
                free_three_to_six_PT += 1
        elif el.hobby_time == 3:
            if el.study_type == 1:
                free_six_to_nine_FT += 1
            else:
                free_six_to_nine_PT += 1
        elif el.hobby_time == 4:
            if el.study_type == 1:
                free_nine_to_twelve_FT += 1
            else:
                free_nine_to_twelve_PT += 1

    # Prepare data for google charts
    free_time = [['0-3', free_zero_to_three_FT, free_zero_to_three_PT], ['3-6', free_three_to_six_FT, free_three_to_six_PT],
                 ['6-9', free_six_to_nine_FT, free_six_to_nine_PT], ['9-12', free_nine_to_twelve_FT, free_nine_to_twelve_PT]]

    # work
    tak_FT = 0
    nie_FT = 0
    tak_PT = 0
    nie_PT = 0
    for el in fd_list:
        if el.work == 1:
            if el.study_type == 1:
                tak_FT += 1
            else:
                tak_PT += 1
        elif el.work == 2:
            if el.study_type == 1:
                nie_FT += 1
            else:
                nie_PT += 1
    # Prepare data for google charts
    working_FT = [['Tak', tak_FT], ['Nie', nie_FT]]
    working_PT = [['Tak', tak_PT], ['Nie', nie_PT]]

    # work_time
    work_time_FT=[]
    work_time_PT = []
    for el in fd_list:
        if el.work == 1:
            if el.study_type == 1:
                work_time_FT.append(el.work_time)
            else:
                work_time_PT.append(el.work_time)
        else:
            continue

    work_time_FT_dict = {i: work_time_FT.count(i) for i in work_time_FT}
    for key in range(1, 9):
        if key not in work_time_FT_dict:
            work_time_FT_dict[key] = 0

    work_time_PT = [i for i in work_time_PT if i != 0]
    work_time_PT_dict = {i: work_time_PT.count(i) for i in work_time_PT}
    for key in range(1, 9):
        if key not in work_time_PT_dict:
            work_time_PT_dict[key] = 0

    work_time = [['1/5', work_time_FT_dict[1], work_time_PT_dict[1]], ['1/4', work_time_FT_dict[2], work_time_PT_dict[2]],
                 ['2/5', work_time_FT_dict[3], work_time_PT_dict[3]], ['1/2', work_time_FT_dict[4], work_time_PT_dict[4]],
                 ['3/5', work_time_FT_dict[5], work_time_PT_dict[5]], ['3/4', work_time_FT_dict[6], work_time_PT_dict[6]],
                 ['4/5', work_time_FT_dict[7], work_time_PT_dict[7]], ['Pełny etat', work_time_FT_dict[8], work_time_PT_dict[8]]]

    # work consistent with education
    yes_FT = 0
    no_FT = 0
    yes_PT = 0
    no_PT = 0
    for el in fd_list:
        if el.work == 1:
            if el.study_type == 1:
                if el.work_vs_study == 1:
                    yes_FT += 1
                else:
                    no_FT += 1
            else:
                if el.work_vs_study == 1:
                    yes_PT += 1
                else:
                    no_PT += 1
        else:
            continue
    # Prepare data for google charts
    consistent_FT = [['Tak', yes_FT], ['Nie', no_FT]]
    consistent_PT = [['Tak', yes_PT], ['Nie', no_PT]]

    # study_and_work
    definitely_yes = 0
    rather_yes = 0
    no_opinion = 0
    rather_no = 0
    definitely_no = 0
    for el in fd_list:
        if el.study_and_work == 1:
            definitely_yes += 1
        elif el.study_and_work == 2:
            rather_yes += 1
        elif el.study_and_work == 3:
            no_opinion += 1
        elif el.study_and_work == 4:
            rather_no += 1
        elif el.study_and_work == 5:
            definitely_no += 1
    # Prepare data for google charts
    study_and_work = [['Zdecydowanie tak', definitely_yes], ['Raczej tak', rather_yes], ['Nie mam zdania', no_opinion],
                      ['Raczej nie', rather_no], ['Zdecydowanie nie', definitely_no]]


    return render_template('result.html', sex=sex, type=type, study=study, free_time=free_time, working_FT=working_FT,
                           working_PT=working_PT, work_time=work_time, consistent_FT=consistent_FT,
                           consistent_PT=consistent_PT, study_and_work=study_and_work)


if __name__ == "__main__":
    app.debug = True
    app.run()

